package com.android.countryapilivedata

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.countryapilivedata.databinding.CountryLayoutBinding
import com.android.countryapilivedata.model.Country


class RecyclerViewAdapter() : RecyclerView.Adapter<RecyclerViewAdapter.CountryViewHolder>() {

    private var countryList: MutableList<Country> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
        return CountryViewHolder(
            CountryLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        holder.bind()
    }


    override fun getItemCount(): Int = countryList.size


    inner class CountryViewHolder(private val binding: CountryLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {
            val model: Country = countryList[absoluteAdapterPosition]
            binding.title.text = model.name
            binding.image.loadSvg(model.photoUrl)
        }

    }

    fun setData(list: MutableList<Country>) {
        this.countryList.clear()
        this.countryList.addAll(list)
        notifyDataSetChanged()
    }

    fun clearData() {
        this.countryList.clear()
        notifyDataSetChanged()

    }
}