package com.android.countryapilivedata

import com.android.countryapilivedata.model.Country
import retrofit2.Response
import retrofit2.http.GET


interface CountryApi {

    @GET("rest/v2/all?fields=name;flag")
    suspend fun listCountries(): Response<List<Country>>

}