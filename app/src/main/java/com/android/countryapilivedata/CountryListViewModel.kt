package com.android.countryapilivedata

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.countryapilivedata.model.Country
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CountryListViewModel : ViewModel() {

    private var fetchedCountries = MutableLiveData<List<Country>>().apply {
        mutableListOf<Country>()
    }

    val _countryLiveData: LiveData<List<Country>> = fetchedCountries

    private var downloadingLiveData = MutableLiveData<Boolean>()

    val _downloadLiveData: LiveData<Boolean> = downloadingLiveData


    fun init() {
        CoroutineScope(Dispatchers.IO).launch {
            populateList()
        }
    }

    private suspend fun populateList() {
        downloadingLiveData.postValue(true)

        val result = RetrofitService.retrofit().listCountries()
        if (result.isSuccessful) {
            fetchedCountries.postValue(result.body() as MutableList<Country>)
        }
        downloadingLiveData.postValue(false)
    }
}
