package com.android.countryapilivedata

import android.net.Uri
import android.widget.ImageView
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou

fun ImageView.loadSvg(url: String?) {
    GlideToVectorYou
        .init()
        .with(this.context)
        .setPlaceHolder(R.drawable.ic_launcher_background, R.drawable.ic_launcher_foreground)
        .load(Uri.parse(url), this)
}