package com.android.countryapilivedata.model

import com.google.gson.annotations.SerializedName

data class Country(
    var name: String = "",
    @SerializedName("flag") var photoUrl: String = ""
)
