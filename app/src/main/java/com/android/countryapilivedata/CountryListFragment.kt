package com.android.countryapilivedata

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.android.countryapilivedata.databinding.CountryListFragmentBinding

class CountryListFragment : Fragment() {


    private val viewModel: CountryListViewModel by viewModels()
    private lateinit var binding: CountryListFragmentBinding
    private lateinit var adapter: RecyclerViewAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = CountryListFragmentBinding.inflate(
            inflater, container, false
        )
        init()
        return binding.root
    }

    private fun init() {
        initRecyclerView()

        viewModel.init()

        observer()

        binding.swipeRefresher.setOnRefreshListener {
            adapter.clearData()
            viewModel.init()
        }
    }

    private fun initRecyclerView() {
        adapter = RecyclerViewAdapter()
        binding.recyclerView.layoutManager = GridLayoutManager(context, 3)
        binding.recyclerView.adapter = adapter
    }

    private fun observer() {
        viewModel._downloadLiveData.observe(viewLifecycleOwner, {
            binding.swipeRefresher.isRefreshing = it
        })

        viewModel._countryLiveData.observe(viewLifecycleOwner, {
            adapter.setData(it.toMutableList())
        })

    }

}